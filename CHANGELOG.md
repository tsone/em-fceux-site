# Changelog for em-fceux-site

## [1.2.1] - 2023-02-06

### Changed

- Migrated to em-fceux 2.2.0.

## [1.2.0] - 2022-06-01

### Changed

- Migrated to em-fceux 2.1.0 (FCEUX fork on github).

## [1.1.1] - 2020-10-04

### Changed

- Simplify Apache htaccess file.

### Fixed

- Firefox checkbox autocomplete messed up app state.
- Menus not hidden upon first play of a game.

## [1.1.0] - 2020-10-03

### Changed

- Upgrade to em-fceux 2.0.0.

### Fixed

- Missing controller 2 input handling (issue #38).

## [1.0.3] - 2020-05-25

### Added

- Hide menus when game canvas is focused.

## [1.0.2] - 2020-04-26

### Added

- File selection to add a game.

## [1.0.1] - 2020-03-15

### Added

- Initial release.
